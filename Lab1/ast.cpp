#include "ast.h"
#include <QDebug>


AbSyTree::TYPE AbSyTree::Type::convertTypeQ(QString name)
{
    if(name == "natural") return AbSyTree::TYPE::NATURAL;
    else if(name == "file") return AbSyTree::TYPE::FILE;
    else if(name == "catalogue") return AbSyTree::TYPE::CATALOGUE;
    else if(name == "url") return AbSyTree::TYPE::URL;
}

QTreeWidgetItem* AbSyTree::VariableDecl::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    QTreeWidgetItem* ident = new QTreeWidgetItem();
    item->setText(0,"VarDecl");
    ident->setText(0,id);
    item->addChild(type->printTree());
    item->addChild(ident);
    return item;
}

QString AbSyTree::Type::convertType(AbSyTree::TYPE type)
{
    if(type == AbSyTree::TYPE::NATURAL) return "natural";
    else if(type == AbSyTree::TYPE::FILE) return "file";
    else if(type == AbSyTree::TYPE::CATALOGUE) return "catalogue";
    else if(type == AbSyTree::TYPE::URL) return "url";
}


QTreeWidgetItem *AbSyTree::File::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,convertType(type));
    return item;
}

QString AbSyTree::File::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    return AbSyTree::Type::convertType(type);
}

QString AbSyTree::VariableDecl::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE>> &symbt)
{
    if(!symbt.contains(id))
    {
        QMap<QString, AbSyTree::TYPE> info;
        info.insert("",AbSyTree::Type::convertTypeQ(type->evaluate(symbt)));
        symbt.insert(id,info);
        return "";
    }

    throw InterpExept("Variable is already declared!");
}

QTreeWidgetItem *AbSyTree::Nat::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,convertType(type));
    return item;
}

QString AbSyTree::Nat::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    return AbSyTree::Type::convertType(type);
}

QTreeWidgetItem *AbSyTree::Catal::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,convertType(type));
    return item;
}

QTreeWidgetItem *AbSyTree::Url::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,convertType(type));
    return item;
}

QString AbSyTree::Catal::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    return AbSyTree::Type::convertType(type);
}


QString AbSyTree::Url::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    return AbSyTree::Type::convertType(type);
}


QTreeWidgetItem *AbSyTree::VariableAssign::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,"=");
    item->addChild(var->printTree());
    item->addChild(rval->printTree());
    return item;
}

QString AbSyTree::VariableAssign::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    QString id = var->id;
    QMap<QString,AbSyTree::TYPE> info;
    if(symbt.contains(id))
    {
        AbSyTree::TYPE vartype = symbt[id].last();
        QString value = rval->evaluate(symbt);
        info.insert(value,vartype);

        bool checkForNumber;
        value.toInt(&checkForNumber);

        if(checkForNumber)
        {
            if(AbSyTree::TYPE::NATURAL != vartype) throw InterpExept("Expected NATURAL but got URL!");
            symbt.insert(id,info);
            return "";
        }
        else
        {
            if(AbSyTree::TYPE::URL == vartype)
            {
                symbt.insert(id,info);
            }
            else
            {
                if(value.endsWith("/"))
                {
                    if(AbSyTree::TYPE::CATALOGUE != vartype) throw InterpExept("Expected CATALOGUE but got FILE!");

                }
                else
                {
                    if(AbSyTree::TYPE::FILE != vartype) throw InterpExept("Expected FILE but got CATALOGUE!");
                }

                symbt.insert(id,info);
            }

            return "";
        }
    }

    throw InterpExept("Variable is not declarated!");
}


QString AbSyTree::Variable::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    if(symbt.contains(id))
        return symbt[id].keys()[0];
    else{
        throw InterpExept("Variable is not declarated!");
    }
}

QTreeWidgetItem *AbSyTree::UrlConst::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,value);
    return item;
}

QString AbSyTree::UrlConst::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    return value;
}

QTreeWidgetItem *AbSyTree::NatConst::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,QString::number(Value));
    return item;
}

QTreeWidgetItem *AbSyTree::Variable::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,id);
    return item;
}

QString AbSyTree::NatConst::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    return QString::number(Value);
}

QTreeWidgetItem *AbSyTree::UnOperator::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,convertUnOpRev(op));
    item->addChild(expr->printTree());
    return item;
}

qint64 dirSize(QString dirPath)
{
    qint64 size = 0;
    QDir dir(dirPath);

    QDir::Filters fileFilters = QDir::Files|QDir::System|QDir::Hidden;
    for(QString filePath : dir.entryList(fileFilters)) {
        QFileInfo fi(dir, filePath);
        size+= fi.size();
    }

    QDir::Filters dirFilters = QDir::Dirs|QDir::NoDotAndDotDot|QDir::System|QDir::Hidden;
    const auto &list = dir.entryInfoList(dirFilters);
    for(const QFileInfo &dir : list) {
      size += dir.size();
      size+= dirSize(dirPath + "/" + dir.fileName());
    }

    return size;
}


QString AbSyTree::UnOperator::convertUnOpRev(AbSyTree::UNOP qtype)
{
    if(qtype == AbSyTree::UNOP::BACKUP) return "backup";
    else if(qtype == AbSyTree::UNOP::DEL) return "delete";
    else if(qtype == AbSyTree::UNOP::SIZE) return "size";
}

QTreeWidgetItem *AbSyTree::VariableInit::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,"VarInit");
    item->addChild(decl->printTree());
    item->addChild(assign->printTree());
    return item;
}

QString AbSyTree::VariableInit::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    decl->evaluate(symbt);
    assign->evaluate(symbt);
    return "";
}

QTreeWidgetItem *AbSyTree::BinOperator::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,op);
    item->addChild(left->printTree());
    item->addChild(right->printTree());
    return item;
}

QString AbSyTree::BinOperator::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    QString lval = left->evaluate(symbt);
    QString rval = right->evaluate(symbt);
    bool r,l;
    int ilval = lval.toInt(&l);
    int irval = rval.toInt(&r);
    if(!(l&&r))
    {
        if(op == "copy" && !l && !r){
            QFile file(lval);
            QDir dir(rval);
            if(!rval.endsWith("/") || lval.endsWith("/"))
                throw InterpExept("Unexpected parameters for copy");
            if(file.exists() && dir.exists()){
                QFileInfo fileInfo(file.fileName());
                QString filename(fileInfo.fileName());
                qDebug() << dir.path()+filename;
                file.copy(dir.path()+"/"+filename);
                return "1";
            }
            else
                return "0";
        }
        else if(op == "find" && !l && !r){
            QFile file(lval);
            QDir dir(rval);
            if(!rval.endsWith("/") || lval.endsWith("/"))
                throw InterpExept("Unexpected parameters for copy");
            if(file.exists() && dir.exists()){
                QFileInfo fileInfo(file.fileName());
                QString filename(fileInfo.fileName());

                QString targetStr = filename;
                   QFileInfoList hitList;
                   QString directory = rval+"/";
                   QDirIterator it(directory, QDirIterator::Subdirectories);


                   while (it.hasNext()) {
                       QString filename = it.next();
                       QFileInfo file(filename);

                       if (file.isDir()) {
                           continue;
                       }

                       if (file.fileName().contains(targetStr, Qt::CaseInsensitive)) {
                           hitList.append(file);
                       }
                   }
                   QString msg;
                   foreach (QFileInfo hit, hitList) {
                       msg.append(hit.absoluteFilePath()+"\n");
                   }
                   QMessageBox qmsg;
                   qmsg.setText(msg);
                   qmsg.exec();

                return "1";
            }
            else
                return "0";
        }
        else
            throw InterpExept("Invalid type in BinOP");
    }
    if(op == ">")
    {
       if(ilval>irval)
           return "1";
       else
           return "0";
    }
    if(op == "<")
    {
       if(ilval<irval)
           return "1";
       else
           return "0";
    }
    if(op == "==")
    {
       if(ilval==irval)
           return "1";
       else
           return "0";
    }
    if(op == "+")
    {
       return QString::number(ilval+irval);
    }
    if(op == "-")
    {
       return QString::number(ilval-irval);
    }
    if(op == "="){
       AbSyTree::Variable* var = dynamic_cast<AbSyTree::Variable*>(left);
       AbSyTree::VariableAssign assign(var,right);
       assign.evaluate(symbt);
       return "";
    }

    throw InterpExept("Unknow operation");
}

QTreeWidgetItem *AbSyTree::IfStatement::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,"ifstmt");
    item->addChild(expr->printTree());
    item->addChild(body->printTree());
    if (elsestmt) item->addChild(elsestmt->printTree());
    return item;
}

QString AbSyTree::IfStatement::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    if(expr->evaluate(symbt).toInt()){
        body->evaluate(symbt);
    }
    else if(elsestmt){
        elsestmt->evaluate(symbt);
    }
    return "";
}

QString AbSyTree::UnOperator::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt){
    QString val = expr->evaluate(symbt);
    bool checkForNumber;
    val.toInt(&checkForNumber);
    if(checkForNumber)
    {
        throw InterpExept("Invalid argument for UnOp");
    }
    bool isCatal = val.endsWith("/");
    if(op == AbSyTree::UNOP::DEL)
    {
        if(isCatal){
            QDir dir(val);
            if(dir.exists())
            {
                dir.removeRecursively();
                return "1";
            }
            else{
                return "0";
            }
        }
        else{
            QFile file(val);
            if(file.exists())
            {
                file.remove();
                return "1";
            }
            else{
                return "0";
            }
        }
    }
    if(op == AbSyTree::UNOP::BACKUP)
    {
        if(isCatal)
            throw InterpExept("Expected FILE in backup but get CATALOGUE");
        QFile file(val);
        if(file.exists())
        {
            QDir b(QDir::current());
            b.mkdir("backup");

            QFileInfo fileInfo(file.fileName());
            QString filename(fileInfo.fileName());
            qDebug() << QDir::currentPath() + "/backup/" + filename;
            file.copy(QDir::currentPath() + "/backup/" + filename);
            return "1";
        }
        else
            return "0";
    }
    if(op == AbSyTree::UNOP::SIZE)
    {
        if(isCatal){
            QDir dir(val);
            if(dir.exists())
            {
                return QString::number(dirSize(val));
            }
            else{
                return "0";
            }
        }
        else{
            QFile file(val);
            if(file.exists())
            {
                return QString::number(file.size());
            }
            else{
                return "0";
            }
        }
    }
}


QTreeWidgetItem *AbSyTree::Body::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,"Body");
    for(int i = 0; i < subtree.size();i++)
    {
        item->addChild(subtree[i]->printTree());
    }
    return item;
}

QString AbSyTree::Body::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    while(!subtree.isEmpty())
    {
        subtree.takeFirst()->evaluate(symbt);
    }
    return "";
}

QTreeWidgetItem *AbSyTree::EsleStatement::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,"ElseStmt");
    item->addChild(elsebd->printTree());
    return item;
}

QString AbSyTree::EsleStatement::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    elsebd->evaluate(symbt);
    return "";
}


QTreeWidgetItem *AbSyTree::ForStatement::printTree()
{
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,"ForStmt");
    item->addChild(init->printTree());
    item->addChild(test->printTree());
    item->addChild(update->printTree());
    item->addChild(stmts->printTree());

    return item;
}

QString AbSyTree::ForStatement::evaluate(QMap<QString, QMap<QString, AbSyTree::TYPE> > &symbt)
{
    init->evaluate(symbt);
    while(test->evaluate(symbt).toInt()){
        AbSyTree::Body tmp = *stmts;
        stmts->evaluate(symbt);
        *stmts = tmp;
        update->evaluate(symbt);
    }
    return "";
}
