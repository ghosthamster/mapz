#ifndef AST_H
#define AST_H

#include <QTreeWidgetItem>
#include <QException>
#include <QDir>
#include <QFile>
#include <QDirIterator>
#include <QMessageBox>

class InterpExept:public QException
{
private:
    QString Msg;
public:
    void raise() const override {throw *this;}
    InterpExept(QString msg):Msg{msg}{}
    QString What(){return Msg;}
    InterpExept* clone() const override {return new InterpExept(*this);}
};

namespace AbSyTree {
    enum class UNOP {DEL,BACKUP, SIZE};
    enum class TYPE {FILE,CATALOGUE,URL,NATURAL};

    class Nod
    {
    public:
        virtual QTreeWidgetItem* printTree() = 0;
        virtual QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt) = 0;
        virtual ~Nod(){}
    };

    class Expression:public Nod
    {
    public:
        QTreeWidgetItem * printTree(){}
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt){}
        ~Expression(){}
    };

    class Statement:public Nod
    {
    public:
        QTreeWidgetItem* printTree(){}
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt){}
        ~Statement(){}
    };

    class Body:public Nod
    { 
    public:
        QList<Nod*> subtree;
        QTreeWidgetItem* printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        ~Body(){}
    };

    class Type:public Nod
    {
    public:
        static AbSyTree::TYPE convertTypeQ(QString);
        static QString convertType(AbSyTree::TYPE type);
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt){}
        QTreeWidgetItem* printTree(){}
        ~Type(){}
    };

    class Variable:public Expression
    {
    public:
        QString id;
        Variable(QString ident):id{ident}{}
        Variable(){}
        QTreeWidgetItem* printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        ~Variable(){}
    };

    class VariableDecl:public Nod
    {
    public:
        QString id;
        Type* type;
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        VariableDecl(QString ID, Type* TYpe):id{ID},type{TYpe}{}
        QTreeWidgetItem* printTree();
        VariableDecl(){}
        ~VariableDecl(){}
    };

    class VariableAssign:public Statement
    {
    public:
        Variable* var;
        Expression* rval;
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        VariableAssign(Variable* Var, Expression* Rval):var{Var},rval{Rval}{}
        QTreeWidgetItem *printTree();
        VariableAssign(){}
        ~VariableAssign(){}
    };

    class VariableInit:public Nod
    {
    public:
        VariableDecl* decl;
        VariableAssign* assign;
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        VariableInit(VariableDecl* Decl, VariableAssign* Assign):decl{Decl},assign{Assign}{}
        QTreeWidgetItem * printTree();
        VariableInit(){}
        ~VariableInit(){}
    };

    class BinOperator:public Expression
    {
    public:
        Expression* left;
        QString op;
        Expression* right;
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        BinOperator(Expression* Left, QString Op, Expression* Right):left{Left},op{Op},right{Right}{}
        QTreeWidgetItem * printTree();
        BinOperator(){}
        ~BinOperator(){}
    };

    class UnOperator:public Expression
    {
    public:
        AbSyTree::UNOP op;
        Expression* expr;
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        QString convertUnOpRev(AbSyTree::UNOP qtype);
        UnOperator(AbSyTree::UNOP Op, Expression* Expression):op{Op},expr{Expression}{}
        QTreeWidgetItem * printTree();
        UnOperator(){}
        ~UnOperator(){}
    };

    class Constant:public Expression
    {
    public:
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt){}
        QTreeWidgetItem* printTree(){}
        ~Constant(){}
    };

    class EsleStatement:public Statement
    {
    public:
        Body* elsebd;
        QTreeWidgetItem * printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        EsleStatement(Body* ElseBD):elsebd{ElseBD}{}
        EsleStatement(){}
        ~EsleStatement(){}
    };

    class IfStatement:public Statement
    {
    public:
        Expression* expr;
        Body* body;
        EsleStatement* elsestmt = nullptr;
        QTreeWidgetItem * printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        IfStatement(Expression* expression,Body* Body):expr{expression},body{Body}{}
        IfStatement(){}
        ~IfStatement(){}
    };

    class ForStatement:public Statement
    {
    public:
        Expression* test;
        Body* stmts;
        Nod* init;
        Expression* update;
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        QTreeWidgetItem * printTree();
        ForStatement(){}
        ~ForStatement(){}
    };

    class File:public Type
    {
    public:
        TYPE type = TYPE::FILE;
        QTreeWidgetItem* printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        ~File(){}
    };

    class Catal:public Type
    {
    public:
        TYPE type = TYPE::CATALOGUE;
        QTreeWidgetItem* printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        ~Catal(){}
    };

    class Nat:public Type
    {
    public:
        TYPE type = TYPE::NATURAL;
        QTreeWidgetItem* printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        ~Nat(){}
    };

    class Url:public Type
    {
    public:
        TYPE type = TYPE::URL;
        QTreeWidgetItem* printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        ~Url(){}
    };

    class NatConst:public Constant
    {
    public:
        int Value;
        TYPE type = TYPE::NATURAL;
        QTreeWidgetItem* printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        NatConst(int val):Value{val}{}
        NatConst(){}
        ~NatConst(){}
    };

    class UrlConst:public Constant
    {
    public:
        QString value;
        TYPE type = TYPE::URL;
        QTreeWidgetItem* printTree();
        QString evaluate(QMap<QString ,QMap<QString , AbSyTree::TYPE>>& symbt);
        UrlConst(QString val):value{val}{}
        UrlConst(){}
        ~UrlConst(){}
    };
}

#endif // AST_H
