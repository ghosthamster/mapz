#include "interpreter.h"

Token::Token(Tag _tag, QString Value)
{
    tag = _tag;
    val = Value;
}

Lexer::Lexer(QString codde)
{
    QVector<QString> keywords;
    QVector<QString> type;
    QVector<QString> operatorsb;
    QVector<QString> operatorsu;

    keywords << "for" << "if" << "else";
    type << "natural" << "url" << "file" << "catalogue";
    operatorsb << ">" << "<" << "==" << "=" << "+" << "-" << "copy" << "find" ;
    operatorsu << "delete" << "backup" << "size" ;

    for(int i = 0; i < keywords.size(); ++i){ Words.insert(keywords[i],Token(Tag::KEYWORD,keywords[i])); }
    for(int i = 0; i < type.size(); ++i){ Words.insert(type[i],Token(Tag::TYPE,type[i])); }
    for(int i = 0; i < operatorsb.size(); ++i){ Words.insert(operatorsb[i],Token(Tag::OPERATORB,operatorsb[i])); }
    for(int i = 0; i < operatorsu.size(); ++i){ Words.insert(operatorsu[i],Token(Tag::OPERATORU,operatorsu[i])); }

    code = codde;
}

QVector<Token> Lexer::GetTokens()
{
    QVector<Token> tokens;
    QString word;
    pos = 0;
    while(pos < code.size())
    {
        word = "";

        if(code[pos].isSpace() || code[pos] == "\n")
        {
            pos++;
            continue;
        }

        if(code[pos].isLetter())
        {
            while (code[pos].isLetterOrNumber())
            {
                word.append(code[pos]);
                pos++;
            }

            tokens.append(Words.value(word,Token(Tag::IDENTIFIER,word)));
            continue;
        }

        if(code[pos].isDigit())
        {
            while (code[pos].isDigit())
            {
                word.append(code[pos]);
                pos++;
            }

            tokens.append(Token(Tag::LITERAL,word));
            continue;
        }

        if(code[pos] == "\"")
        {
            do
            {
                word.append(code[pos]);
                pos++;
            }while (code[pos] != "\"" && pos < code.size());

            pos++;
            word.remove(0,1);

            tokens.append(Token(Tag::LITERAL,word));
            continue;
        }

        if(code[pos] == ";" || code[pos] == "{" || code[pos] == "}")
        {
            word.append(code[pos]);
            pos++;
            tokens.append(Token(Tag::SEPARATORS,word));
            continue;
        }

        if(code[pos] == ">" || code[pos] == "<" || code[pos] == "=" || code[pos] == "+" || code[pos] == "-")
        {
            if(code[pos] == '=' && code[pos+1] == '=') { word.append('='); pos++;}
            word.append(code[pos]);
            tokens.append(Token(Tag::OPERATORB,word));
            pos++;
            continue;
        }
        else
        {
            InterpExept exception("Unknown symbol has been found!\nPosition: " + QString::number(pos) + "\nSymbol: " + code[pos]);
            exception.raise();
        }
    }
    return tokens;
}

QList<AbSyTree::Nod*> Parser::newTree(bool isBody)
{
    QList<AbSyTree::Nod*> tree;
    tree.clear();
    while(tokens.size())
    {
        Curr = tokens.takeFirst();
        if(Curr.tag == Tag::TYPE)
        {
            if((tokens.size()) > 2 && tokens[1].val == '=') tree.push_back(prInitr());
            else tree.push_back(prVarDecl());
            NotEmptyAssign("Expected ';' at the end of stmt");
            if(Curr.val != ';') throw InterpExept("Expected ';' at the end of stmt");
        }
        else if(Curr.tag == Tag::IDENTIFIER)
        {
            tree.push_back(prVarAssign());
            NotEmptyAssign("Expected ';' at the end of stmt");
            if(Curr.val != ';') throw InterpExept("Expected ';' at the end of stmt");
        }
        else if(Curr.val == "if")
        {
            tree.push_back(prIf());
        }
        else if(Curr.val == "for")
        {
            tree.push_back(prFor());
        }
        else if(Curr.val == "}")
        {
            break;
        }
        else if (Curr.tag == Tag::OPERATORU)
        {
            tree.push_back(prUnOp());
            if(Curr.val != ';') throw InterpExept("Expected ';' at the end of stmt");
        }

    }

    return tree;
}

AbSyTree::VariableDecl* Parser::prVarDecl()
{
    AbSyTree::VariableDecl* node = new AbSyTree::VariableDecl();
    node->type = convertType(Curr.val);
    NotEmptyAssign("Missing ID after TYPE");
    if(Curr.tag == Tag::IDENTIFIER)
    {
        node->id = Curr.val;
        return node;
    }
    throw InterpExept("Missing ID after Type");
}

AbSyTree::VariableAssign* Parser::prVarAssign()
{
    AbSyTree::VariableAssign* node = new AbSyTree::VariableAssign();
    node->var = new AbSyTree::Variable(Curr.val);
    NotEmptyAssign("Missing '=' after ID");
    if(Curr.val == "=")
    {
        node->rval = prExpr();
        return node;
    }
    throw InterpExept("Missing '=' after ID");
}

AbSyTree::VariableInit *Parser::prInitr()
{
    AbSyTree::VariableInit* node = new AbSyTree::VariableInit();
    node->decl = prVarDecl();
    node->assign = prVarAssign();
    return node;
}

AbSyTree::Expression *Parser::prExpr()
{
    NotEmptyAssign("Expected expression");
    if(!tokens.isEmpty() && (tokens.first().tag == Tag::OPERATORB))
    {
        AbSyTree::Expression* expr = nullptr;
        QString op = tokens.takeFirst().val;
        tokens.push_front(Curr);
        expr = prBinOp(op);
        return expr;
    }
    else if(Curr.tag == Tag::IDENTIFIER) return new AbSyTree::Variable(Curr.val);
    else if(Curr.tag == Tag::LITERAL) return  prConst();
    else if(Curr.tag == Tag::OPERATORU) return  prUnOp();

    throw InterpExept("Expected expression");
}

AbSyTree::BinOperator *Parser::prBinOp(QString oper)
{
    AbSyTree::BinOperator *node = new AbSyTree::BinOperator();
    node->left = prExpr();
    node->op = oper;
    node->right = prExpr();
    return node;
}

AbSyTree::Body *Parser::prBody()
{
    AbSyTree::Body* node = new AbSyTree::Body();
    NotEmptyAssign("Expected '{' at body start");
    if(Curr.val == '{')
    {
        node->subtree = newTree(true);
        if(Curr.val == '}') return node;
        throw InterpExept("Expected '}' at body end");
    }
    throw InterpExept("Expected '{' at body start");
}

AbSyTree::IfStatement *Parser::prIf()
{
    AbSyTree::IfStatement* node = new AbSyTree::IfStatement();
    node->expr = prExpr();
    node->body = prBody();

    if(!tokens.isEmpty() && tokens.front().val == "else")
    {
        tokens.pop_front();
        node->elsestmt = new AbSyTree::EsleStatement();
        node->elsestmt->elsebd = prBody();
    }
    return node;
}

AbSyTree::ForStatement *Parser::prFor()
{
    AbSyTree::ForStatement* node = new AbSyTree::ForStatement();
    NotEmptyAssign("Expected Initialization or Assigment in FOR stmt");
    if(Curr.tag == Tag::TYPE) node->init = prInitr();
    else if(Curr.tag == Tag::IDENTIFIER) node->init = prVarAssign();
    node->test = prExpr();
    node->update = prExpr();
    node->stmts = prBody();
    return node;
}

AbSyTree::Constant *Parser::prConst()
{
    QRegExp re("\d");
    if(re.exactMatch(Curr.val)) return new AbSyTree::NatConst(Curr.val.toInt());
    return new AbSyTree::UrlConst(Curr.val);
}

AbSyTree::UnOperator *Parser::prUnOp()
{
    AbSyTree::UnOperator* node = new AbSyTree::UnOperator();
    node->op = convertUnOp(Curr.val);
    node->expr = prExpr();
    return node;
}

void Parser::printTree(QList<AbSyTree::Nod*> tree,QTreeWidget *trwdg)
{
    trwdg->clear();
    while(!tree.isEmpty())
    {
        trwdg->addTopLevelItem(tree.first()->printTree());
        tree.pop_front();
    }
}

AbSyTree::Type *Parser::convertType(QString qtype)
{
    if(qtype == "natural") return new AbSyTree::Nat();
    else if(qtype == "file") return  new AbSyTree::File();
    else if(qtype == "catalogue") return new AbSyTree::Catal;
    else if(qtype == "url") return new AbSyTree::Url();
}

AbSyTree::UNOP Parser::convertUnOp(QString qtype)
{
    if(qtype == "backup") return AbSyTree::UNOP::BACKUP;
    else if(qtype == "delete") return  AbSyTree::UNOP::DEL;
    else if(qtype == "size") return AbSyTree::UNOP::SIZE;
}

void Parser::NotEmptyAssign(QString err)
{
    if (tokens.isEmpty()) throw InterpExept(err);
    Curr = tokens.takeFirst();
}

Parser::Parser(QVector<Token> tokenss)
{
    tokens = tokenss;
}


void Interpreter::Evaluate()
{
    while(!tree.isEmpty())
    {
        tree.takeFirst()->evaluate(symbtable);
    }

}
