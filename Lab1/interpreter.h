#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <QString>
#include <QMap>
#include <QVector>
#include "ast.h"


enum class Tag {IDENTIFIER,KEYWORD,TYPE,LITERAL,OPERATORB,SEPARATORS,OPERATORU};

class Token
{
public:
    Tag tag;
    QString val;
    Token(Tag _tag, QString Value);
    Token(){}
    ~Token(){}
};

class Lexer
{
private:
    QString code;
    QMap<QString,Token> Words;
    int pos;
    char cur;

public:
    Lexer(QString codde);
    QVector<Token> GetTokens();
};

class Parser
{
public:
    QVector<Token> tokens;
    Token Curr;

    QList<AbSyTree::Nod*> newTree(bool isBody);
    QString prId();
    AbSyTree::VariableDecl* prVarDecl();
    AbSyTree::VariableAssign* prVarAssign();
    AbSyTree::VariableInit* prInitr();
    AbSyTree::Expression* prExpr();
    AbSyTree::Constant *prConst();
    AbSyTree::UnOperator* prUnOp();
    AbSyTree::BinOperator* prBinOp(QString oper);
    AbSyTree::Body* prBody();
    AbSyTree::IfStatement* prIf();
    AbSyTree::ForStatement* prFor();
    void NotEmptyAssign(QString err);
    void printTree(QList<AbSyTree::Nod*> tree,QTreeWidget *trwdg);
    static AbSyTree::Type* convertType(QString qtype);
    static AbSyTree::UNOP convertUnOp(QString qtype);
    Parser(QVector<Token> tokenss);
};

class Interpreter
{
public:
    QList<AbSyTree::Nod*> tree;
    QMap<QString ,QMap<QString , AbSyTree::TYPE>> symbtable;
    void Evaluate();
    Interpreter(QList<AbSyTree::Nod*> Tree):tree{Tree}{}
    ~Interpreter(){}
};

#endif // INTERPRETER_H
