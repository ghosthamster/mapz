#ifndef GAME_H
#define GAME_H
#include <QString>
#include <QVector>
#include "music.h"
#include <QMessageBox>

class Player
{
    private:
    Player()
    {
        lvl = 0;
        hp = 100;
        exp = 0;
        dmg = 10;
        def = 0;
        gold = 1000;
    }
    static Player* player;
public:
    Player(Player &other) = delete;
    void operator=(const Player &) = delete;
    static Player *GetInstance()
    {
        if(player==nullptr){
            player = new Player();
        }
        return player;
    }
    int lvl;
    int hp;
    int exp;
    int dmg;
    int def;
    int gold;
    QVector<QString> items;
    QVector<QString> quests;
};



class Shop{
    void takemoney(int price ){
        Player::GetInstance()->gold -= price;
    }
    void giveitem(QString name){
        if(name.size()!=0)
            Player::GetInstance()->items.append(name);
    }
public:
    void buy(int price,QString name){
        takemoney(price);
        giveitem(name);
    }
};
class NPC{
private:
    void playSound(){
        MusicManager::GetInstance()->set("C:/Users/admin/Pictures/game/bogdan.mp3");
        MusicManager::GetInstance()->play();
    }
    void stopSound(){
        MusicManager::GetInstance()->stop();
    }
    void sayText(){
        QMessageBox msgBox;
        msgBox.setText("Ось твій квест..");
        msgBox.exec();
    }
    void giveQvest(){
         Player::GetInstance()->quests.append("some quest");
    }
public:
    void comunicate(){
        playSound();
        sayText();
        giveQvest();
        stopSound();
    }
};
class City{
protected:
    Shop* shop;
    NPC* npc;
public:
    City(){
        Shop* shop= new Shop;
        NPC* npc= new NPC;
    }
    ~City(){
        delete shop;
        delete npc;
    }
    void buyItem(int price,QString item){
        shop->buy(price,item);
    }
    void talkWithNPC(){
        npc->comunicate();
    }
};


class Potion{
public:
  virtual ~Potion() {}
  virtual void Use() const = 0;
};

class HealPotion:public Potion{
public:
    void Use()const override {

  }
};

class DMGPotion:public Potion{
public:
    void Use()const override {

  }
};


class Decorator : public Potion {

 protected:
  Potion* component_;

 public:
  Decorator(Potion* component) : component_(component) {
  }
  void Use() const override {
    ;
  }
};

class HealDecorator : public Decorator {
 public:
  HealDecorator(Potion* component) : Decorator(component) {
  }
    void Use() const override {
    Decorator::Use();
    Player::GetInstance()->hp += 10;
  }
};

class DMGDecorator : public Decorator {
 public:
  DMGDecorator(Potion* component) : Decorator(component) {
  }
    void Use() const override {
    Decorator::Use();
    Player::GetInstance()->dmg += 10;
  }
};

#endif // GAME_H
