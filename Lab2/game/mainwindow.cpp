#include "mainwindow.h"
#include "ui_mainwindow.h"

Player* Player::player=nullptr;;





MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->shop->hide();
    ui->close->hide();
    ui->h_money->hide();
    ui->buycocos->hide();
    ui->buysword->hide();
    ui->inventory->hide();
    ui->buyheal->hide();
    ui->buyDMG->hide();
    music = MusicManager::GetInstance();
    pl = Player::GetInstance();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_enterForest_clicked()
{

    ForestLocation *loc = new ForestLocation();
    AbstractEnemy *enemy = loc->createEnemy();
    en = enemy;
    for(int i=0; i < 10 ;++i)
    {
        enArr.append(en->clone());
    }
    AbstractBackground * back = loc->createBackground();
    delete loc;
    ui->stackedWidget->setCurrentIndex(back->background);
    //set stats
    ui->lvl->setText(QString::number(enemy->Level));
    ui->h_hp->setText(QString::number(pl->hp));
    ui->h_dmg->setText(QString::number(pl->dmg));
    ui->h_exp->setText(QString::number(pl->exp));
    ui->h_lvl->setText(QString::number(pl->lvl));


}

void MainWindow::on_run_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_enterCastle_clicked()
{
    CastleLocation *loc = new CastleLocation();
    AbstractEnemy *enemy = loc->createEnemy();
    en = enemy;
    AbstractBackground * back = loc->createBackground();
    delete loc;
    ui->stackedWidget->setCurrentIndex(back->background);
    //set stats
    ui->lvl_2->setText(QString::number(enemy->Level));
    ui->h_hp_2->setText(QString::number(pl->hp));
    ui->h_dmg_2->setText(QString::number(pl->dmg));
    ui->h_exp_2->setText(QString::number(pl->exp));
    ui->h_lvl_2->setText(QString::number(pl->lvl));

}

void MainWindow::on_run_2_clicked()
{
     ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_damage_clicked()
{
    en->HP = en->HP-((pl->dmg-en->Def)>0?(pl->dmg-en->Def):0);
    ui->batle_log->append("Ви нанесли удар у ворога "+ QString::number(en->HP)+ "хп");
    if(en->HP<=0){
        ui->batle_log->append("Ворог вбитий\n Створено нового ворога");
        pl->exp += en->Exp;
        ui->h_exp->setText(QString::number(pl->exp));
        delete en;
        if(enArr.size()<2){
            for(int i=0; i < 10 ;++i)
            {
                enArr.append(enArr[0]->clone());
            }
        }
        en = enArr.takeLast();
    }
}

void MainWindow::on_enterArena_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    ArenaEnemyCreator enemyCreator;
    QVector<ArenaEnemy*> arr;
    arr.append(enemyCreator.createEnemy());
    for(int i=0; i < 100 ;++i)
    {
        arr.append(arr[0]);
    }

}

void MainWindow::on_run_3_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_enterCity_clicked()
{
    ui->stackedWidget->setCurrentIndex(4);
}



void MainWindow::on_map_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);

}

void MainWindow::on_seller_clicked()
{
    ui->seller->hide();
    ui->seller_label->hide();
    ui->shop->show();
    ui->close->show();
    ui->buycocos->show();
    ui->bogdan->hide();
    ui->h_money->show();
    ui->buysword->show();
    ui->inventory->show();
    ui->buyheal->show();
    ui->buyDMG->show();
    ui->h_money->setText(QString::number(pl->gold));
}

void MainWindow::on_close_clicked()
{
    ui->seller->show();
    ui->seller_label->show();
    ui->shop->hide();
    ui->close->hide();
    ui->buycocos->hide();
    ui->bogdan->show();
    ui->h_money->hide();
    ui->buysword->hide();
    ui->inventory->hide();
    ui->buyheal->hide();
    ui->buyDMG->hide();
}

void MainWindow::on_buycocos_clicked()
{
    pl->hp += 5;
    city.buyItem(5,"");
    ui->h_money->setText(QString::number(pl->gold));
}

void MainWindow::loadItem(){
    ui->inventory->clear();
    for(int i=0;i<pl->items.size();++i)
    {
        QListWidgetItem *newItem = new QListWidgetItem;
        newItem->setText(pl->items[i]);
        ui->inventory->insertItem(i, newItem);
    }
}

void MainWindow::on_bogdan_clicked()
{
    city.talkWithNPC();
}

void MainWindow::on_music_clicked()
{

    if(music->qmediaplayer->state()== QMediaPlayer::State::PausedState){
        music->set("C:/Users/admin/Pictures/game/village.mp3");
        music->play();
    }
    else if(music->qmediaplayer->state()== QMediaPlayer::State::PlayingState){
        music->stop();
    }
    else{
        music->set("C:/Users/admin/Pictures/game/village.mp3");
        music->play();
    }
}


void MainWindow::on_buysword_clicked()
{
    city.buyItem(5,"sword");
    ui->h_money->setText(QString::number(pl->gold));
    loadItem();
}

void MainWindow::on_buyheal_clicked()
{
     city.buyItem(10,"");
     ui->h_money->setText(QString::number(pl->gold));
     Potion* simple = new HealPotion;
     Potion * decorator1 = new HealDecorator(simple);
     decorator1->Use();
     delete simple;
     delete decorator1;
}

void MainWindow::on_buyDMG_clicked()
{
    city.buyItem(10,"");
    ui->h_money->setText(QString::number(pl->gold));
    Potion* simple = new DMGPotion;
    Potion * decorator1 = new DMGDecorator(simple);
    decorator1->Use();
    delete simple;
    delete decorator1;
}
