#ifndef MUSIC_H
#define MUSIC_H
#include "mutex"
#include <QMediaPlayer>



class MusicManager
{
private:
    static MusicManager *musicmanager;
    static std::mutex mut;


    MusicManager(MusicManager&);
    void operator=(MusicManager&);
protected:
    MusicManager(QString name);
    MusicManager();
public:
    QMediaPlayer *qmediaplayer;
    static MusicManager *GetInstance();
    void set(QString name);
    void play();
    void stop();
};


#endif // MUSIC_H
