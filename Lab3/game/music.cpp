#include "music.h"

MusicManager* MusicManager::musicmanager = nullptr;
std::mutex MusicManager::mut;


MusicManager::MusicManager()
{
    qmediaplayer = new QMediaPlayer();
}

MusicManager *MusicManager::GetInstance()
{
    if(musicmanager == nullptr)
    {
        std::lock_guard<std::mutex> lock(mut);
        if (musicmanager == nullptr)
        {
            musicmanager = new MusicManager();
        }
    }
    return musicmanager;
}

void MusicManager::set(QString name)
{
    if(!name.size()){
        qmediaplayer->setMedia(QUrl::fromLocalFile("C:/Users/admin/Pictures/game/bogdan.mp3"));
    }
    else{
        qmediaplayer->setMedia(QUrl::fromLocalFile(name));
    }
}

void MusicManager::play()
{
    qmediaplayer->setVolume(50);
    qmediaplayer->play();
}

void MusicManager::stop()
{
    qmediaplayer->pause();
}
