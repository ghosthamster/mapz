#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "fight.h"
#include "game.h"
#include "music.h"
#include <QVector>
#include <QListWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    Player *pl;
    AbstractEnemy *en;
    QVector<AbstractEnemy *> enArr;
    MusicManager* music;
    City city;
    Memento *meme;
    Context *context;
    Server *serv;
    Clien *cl;
    Publisher *publisher;
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_enterForest_clicked();

    void on_run_clicked();

    void on_enterCastle_clicked();

    void on_run_2_clicked();

    void on_damage_clicked();

    void on_enterArena_clicked();

    void on_run_3_clicked();

    void on_enterCity_clicked();

    void on_map_clicked();

    void on_seller_clicked();

    void on_close_clicked();

    void on_buycocos_clicked();

    void on_bogdan_clicked();

    void on_music_clicked();

    void on_buysword_clicked();

    void loadItem();
    void on_buyheal_clicked();

    void on_buyDMG_clicked();

    void on_load_clicked();

    void on_save_clicked();

    void on_comboBox_currentIndexChanged(int index);

    void on_stats_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
