#ifndef LOGIC_H
#define LOGIC_H
#include <QString>
#include <QMap>
#include <QPixmap>

class AbstractEnemy
{
public:
    int HP;
    int Dmg;
    int Def;
    int Level;
    int Exp;
    QPixmap* sprite;

    QMap<QString,int> GetStats();
    QPixmap* getSprite();
    virtual AbstractEnemy* clone() const= 0;
};

class AbstractBackground
{
public:
    int background;
};

class ForestBackground: public AbstractBackground
{
public:
    ForestBackground()
    {
        background = 1;
    }
};

class CastleBackground: public AbstractBackground
{
public:
    CastleBackground()
    {
        background = 2;
    }
};

class ForestEnemy: public AbstractEnemy
{
public:
    ForestEnemy()
    {
        HP = 50;
        Dmg = 10;
        Def = 5;
        Level = 1;
        Exp = 10;
        sprite = new QPixmap(); //
    }
    AbstractEnemy* clone() const override{
        return new ForestEnemy();
    }
};


class CastleEnemy: public AbstractEnemy
{
public:
    CastleEnemy()
    {
        HP = 1000;
        Dmg = 100;
        Def = 100;
        Level = 100;
        Exp = 1000;
        sprite = new QPixmap();
    }
    AbstractEnemy* clone() const override{
        return new CastleEnemy();
    }
};

class AbstractLocationFabric
{
public:
    virtual AbstractEnemy *createEnemy() const = 0;
    virtual AbstractBackground *createBackground() const = 0;
};

class ForestLocation: public AbstractLocationFabric
{
public:
    AbstractEnemy *createEnemy()const override {
        return new ForestEnemy();
    }
    AbstractBackground *createBackground()const override {
        return new ForestBackground();
    }
};


class CastleLocation: public AbstractLocationFabric
{
public:
    AbstractEnemy *createEnemy()const override {
        return new CastleEnemy();
    }

    AbstractBackground *createBackground()const override {
        return new CastleBackground();
    }
};
class EnemySprite{
public:
    QPixmap* sprite;
};

class ArenaEnemy{
public:
    int HP;
    int Dmg;
    int Def;
    int Level;
    int Exp;
    EnemySprite *sprite;
};

class ArenaEnemyCreator{
public:
    ArenaEnemy* createEnemy(){
        EnemySprite* sprite = new EnemySprite;
        sprite->sprite =  new QPixmap("C:/Users/admin/Pictures/game/wolf.png");
        ArenaEnemy* ret = new ArenaEnemy;
        ret->sprite = sprite;
        ret->HP = 1000;
        ret->Dmg = 100;
        ret->Def = 100;
        ret->Level = 100;
        ret->Exp = 1000;
        return ret;
    }
};
#endif // LOGIC_H
