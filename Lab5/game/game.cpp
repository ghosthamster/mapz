#include "game.h"
QDataStream &operator<<(QDataStream &out, const Player *s)
{
    out << s->lvl << s->hp << s->exp << s->dmg << s->def << s->gold << s->items << s->quests;
    return out;
}

QDataStream &operator>>(QDataStream &in, Player *s)
{
    in >> s->lvl >> s->hp >> s->exp >> s->dmg >> s->def >> s->gold >> s->items >> s->quests;
    return in;
}
Memento *Player::saveState()
{
    return new Memento("C:/Users/admin/Desktop/saveFile.txt");
}

void Player::restoreState(Memento *m)
{
    m->restore();
}



Memento::Memento(QString filename)
{
    QFile saveFile(filename);
    saveFile.open(QIODevice::WriteOnly);
    QDataStream file(&saveFile);
    file << Player::GetInstance();
}

void Memento::restore()
{
    QFile saveFile("C:/Users/admin/Desktop/saveFile.txt");
    saveFile.open(QIODevice::ReadWrite);
    QDataStream file(&saveFile);
    file >> Player::GetInstance();
}


int GetStats::getStats() const
{
    return server->getStats();
}

int GetStats::execute() const
{
    return getStats();
}

int EditStats::editStats() const
{
    return server->editStats();
}

int EditStats::execute() const
{
    return editStats();
}

void Publisher::notify()
{
    for(Achivment* x:Subs)
    {
        x->upd();
    }
}

void Achivment1::upd()
{
    if(Player::GetInstance()->items.size() == 1)
    {
        QMessageBox msgBox;
        msgBox.setText("Ачівка:мечник");
        msgBox.exec();
    }
}

void Achivment2::upd()
{
    if(Player::GetInstance()->quests.size() == 1)
    {
        QMessageBox msgBox;
        msgBox.setText("Ачівка:Перший квест");
        msgBox.exec();
    }

}
