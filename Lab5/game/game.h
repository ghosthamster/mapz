#ifndef GAME_H
#define GAME_H
#include <QString>
#include <QVector>
#include "music.h"
#include <QMessageBox>
#include <QDataStream>
#include <QString>
#include <QDebug>
#include <QFile>


class Achivment{
public:
    virtual void upd() =0;
    virtual ~Achivment(){}
};

class Achivment1:public Achivment
{
public:
    void upd() override;
    ~Achivment1(){}
};

class Achivment2:public Achivment
{
public:
    void upd() override;
    ~Achivment2(){}
};
class Publisher{
    QVector<Achivment *>Subs;
public:
    void addSub(Achivment * sub){Subs.append(sub);}
    void remSum(Achivment * sub){Subs.removeOne(sub);}
    void notify();
};

class Command{
public:
    virtual int execute()const =0;
    virtual ~Command(){};
};

class Clien{
private:
    Command * command;
public:
    void setCommand(Command *com){command = com;}
    int execute(){return command->execute();}

};
class Server{
int countofenemy;
public:
Server(){countofenemy =0;}
int getStats(){return countofenemy;}
int editStats(){return ++countofenemy;}
};


class GetStats:public Command{
private:
int getStats() const;
Server * server;
public:
int execute() const override;
GetStats(Server *s){server =s;}
~GetStats(){}
};

class EditStats:public Command{
private:
int editStats() const;
Server * server;
public:
int execute() const override;
~EditStats(){}
EditStats(Server *s){server =s;}
};



class Memento
{
  public:
    Memento(QString filename);
    void restore();
};

class Strategy
{
public:
    virtual ~Strategy() {}
    virtual int calDef() const = 0;
    virtual int calDmg() const = 0;
};

class Context
{
private:
    Strategy *strategy_;

public:
    Context(Strategy *strategy = nullptr) : strategy_(strategy)
    {
    }
    ~Context()
    {
        delete this->strategy_;
    }

    void set_strategy(Strategy *strategy)
    {
        delete this->strategy_;
        this->strategy_ = strategy;
    }

    int calDmg() const
    {
        return strategy_->calDmg();
    }
    int calDef() const
    {
        return strategy_->calDef();
    }
};

class easyl:public Strategy
{
public:
    ~easyl() {}
    int calDef() const override{return 3;}
    int calDmg() const override{return 3;}
};
class normall:public Strategy
{
public:
    ~normall() {}
    int calDef() const override{return 2;}
    int calDmg() const override{return 2;}
};
class hardl:public Strategy
{
public:
    ~hardl() {}
    int calDef() const override{return 1;}
    int calDmg() const override{return 1;}
};


class Player
{
    private:
    Player()
    {
        lvl = 0;
        hp = 100;
        exp = 0;
        dmg = 10;
        def = 0;
        gold = 1000;
    }
    static Player* player;
public:
    Memento * saveState();
    void restoreState(Memento *);
    Player(Player &other) = delete;
    void operator=(const Player &) = delete;
    static Player *GetInstance()
    {
        if(player==nullptr){
            player = new Player();
        }
        return player;
    }
    int lvl;
    int hp;
    int exp;
    int dmg;
    int def;
    int gold;
    QVector<QString> items;
    QVector<QString> quests;


    friend QDataStream &operator<<(QDataStream &out, const Player &s);

    friend QDataStream &operator>>(QDataStream &in, Player &s);

};



class Shop{
    void takemoney(int price ){
        Player::GetInstance()->gold -= price;
    }
    void giveitem(QString name){
        if(name.size()!=0)
            Player::GetInstance()->items.append(name);
    }
public:
    void buy(int price,QString name){
        takemoney(price);
        giveitem(name);
    }
};
class NPC{
private:
    void playSound(){
        MusicManager::GetInstance()->set("C:/Users/admin/Pictures/game/bogdan.mp3");
        MusicManager::GetInstance()->play();
    }
    void stopSound(){
        MusicManager::GetInstance()->stop();
    }
    void sayText(){
        QMessageBox msgBox;
        msgBox.setText("Ось твій квест..");
        msgBox.exec();
    }
    void giveQvest(){
         Player::GetInstance()->quests.append("some quest");
    }
public:
    void comunicate(){
        playSound();
        sayText();
        giveQvest();
        stopSound();

    }
};
class City{
protected:
    Shop* shop;
    NPC* npc;
public:
    City(){
        Shop* shop= new Shop;
        NPC* npc= new NPC;
    }
    ~City(){
        delete shop;
        delete npc;
    }
    void buyItem(int price,QString item){
        shop->buy(price,item);
    }
    void talkWithNPC(){
        npc->comunicate();
    }
};


class Potion{
public:
  virtual ~Potion() {}
  virtual void Use() const = 0;
};

class HealPotion:public Potion{
public:
    void Use()const override {

  }
};

class DMGPotion:public Potion{
public:
    void Use()const override {

  }
};


class Decorator : public Potion {

 protected:
  Potion* component_;

 public:
  Decorator(Potion* component) : component_(component) {
  }
  void Use() const override {
    ;
  }
};

class HealDecorator : public Decorator {
 public:
  HealDecorator(Potion* component) : Decorator(component) {
  }
    void Use() const override {
    Decorator::Use();
    Player::GetInstance()->hp += 10;
  }
};

class DMGDecorator : public Decorator {
 public:
  DMGDecorator(Potion* component) : Decorator(component) {
  }
    void Use() const override {
    Decorator::Use();
    Player::GetInstance()->dmg += 10;
  }
};

#endif // GAME_H
